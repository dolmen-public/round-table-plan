import { reactive, ref, watch } from 'vue'

export class Person {
  id: string
  name: string
  shouldSee = new Set<Person['id']>()

  constructor(name: string) {
    this.name = name
    this.id = Date.now().toString()
  }

  static fromJson(jsonPerson: JsonPerson) {
    const person = new Person(jsonPerson.name)
    person.id = jsonPerson.id
    jsonPerson.shouldSee.forEach((id) => person.shouldSee.add(id))
    return person
  }
}

type JsonPerson = {
  id: string
  name: string
  shouldSee: Person['id'][]
}

export const people = reactive<Map<string, Person>>(new Map<string, Person>())

export const focusPerson = ref<Person['id']>('')

watch(people, () => {
  localStorage.setItem(
    'people',
    JSON.stringify(
      [...people.values()].map((p) =>
        Object.assign({}, p, { shouldSee: [...(p.shouldSee?.values() || [])] })
      )
    )
  )
})

async function load() {
  const strPeople = localStorage.getItem('people')
  if (strPeople) {
    const peopleToLaod = JSON.parse(strPeople)
    for (const person of peopleToLaod) {
      people.set(person.id, Person.fromJson(person))
    }
  }
}
load()
