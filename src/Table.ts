import { ref, watch } from 'vue'

export class Table {
  idx: number
  size: number

  get name() {
    return 'Table ' + this.idx
  }

  constructor(idx: number, size: number) {
    this.idx = idx
    this.size = size
  }
}

export const tableSize = ref(5)

watch(
  () => tableSize.value,
  () => {
    localStorage.setItem('tableSize', JSON.stringify(tableSize.value))
  }
)

async function load() {
  const strTableSize = localStorage.getItem('tableSize')
  if (strTableSize) {
    const size = JSON.parse(strTableSize)
    if (Number.isInteger(size) && size) tableSize.value = size
  }
}
load()

export function generateTables(numberOfPeople: number): Table[] {
  if (
    !Number.isInteger(numberOfPeople) ||
    !Number.isInteger(tableSize.value) ||
    tableSize.value < 2
  )
    return []

  const tableNumber = Math.ceil(numberOfPeople / tableSize.value)
  const medianTableSize = Math.ceil(numberOfPeople / tableNumber)

  const tables: Table[] = []

  const rest = numberOfPeople % tableNumber
  for (let tableIdx = 1; tableIdx <= tableNumber; tableIdx++) {
    tables.push(
      new Table(
        tableIdx,
        rest !== 0 && tableIdx > rest ? medianTableSize - 1 : medianTableSize
      )
    )
  }

  return tables

  return []
}
