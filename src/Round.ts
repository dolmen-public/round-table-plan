import { ref, watch } from 'vue'
import type { Person } from './Person'

export type Round = {
  round: string
  tees: ResultTables[]
}

export type ResultTables = {
  no: number
  size: number
  players: Person['id'][]
}

export const numberOfRounds = ref(5)
export const rounds = ref<Round[]>([])

watch(rounds, () => {
  localStorage.setItem('rounds', JSON.stringify(rounds.value))
})

async function load() {
  const strRounds = localStorage.getItem('rounds')
  if (strRounds) {
    rounds.value = JSON.parse(strRounds)
    numberOfRounds.value = rounds.value.length
  }
}
load()
